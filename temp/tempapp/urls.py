from django.urls import path, include

from .views import user_list, add_address

urlpatterns = [
    path('api/get-users-list/', user_list),
    path('api/add-address/', add_address)
]



