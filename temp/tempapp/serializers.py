from rest_framework import serializers
from .models import User, UserAddress


class UserProfile(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = ('first_name', 'last_name',)


class UserProfileForSuopport(serializers.ModelSerializer):

    class Meta:

        model = User
        fields = ('first_name', 'last_name', 'number_of_addresses',)


class AddressSerializers(serializers.ModelSerializer):

    class Meta:

        model = UserAddress
        fields = ('user', 'title', 'latitude', 'longitude',)

