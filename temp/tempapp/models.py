from django.db import models
from django.contrib.auth.models import User as DjangoUser


ADDRESS_CREATOR_CHOICES = (
    ('u', 'user'),
    ('s', 'support'),
)

class UserManager(models.Manager):
    def n_addresses_equal(self, k):
        return super().get_queryset().annotate(num_addr=models.Count('useraddress')).filter(num_addr=k)

    def n_addresses_greater(self, k):
        return super().get_queryset().annotate(num_addr=models.Count('useraddress')).filter(num_addr__gt=k)

    def n_addresses_less(self, k):
        return super().get_queryset().annotate(num_addr=models.Count('useraddress')).filter(num_addr__lt=k)




class User(models.Model):

    django_user = models.OneToOneField(DjangoUser, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length = 15)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def first_name(self):
        return self.django_user.first_name

    @property
    def last_name(self):
        return self.django_user.last_name

    @property
    def number_of_addresses(self):
        addresses = self.useraddress_set.all()
        return addresses.count()

    objects = UserManager()

class SupportCreatedUserAddressManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(creator_type='s')


class UserAddress(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length = 100)
    latitude = models.FloatField()
    longitude = models.FloatField()
    creator_type = models.CharField(max_length=1,
                  choices=ADDRESS_CREATOR_CHOICES)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = models.Manager()
    support_created_objects = SupportCreatedUserAddressManager()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        self.send_notif()


    def send_notif(self):
        print('>>>>>>>>>>>>>>>>>>>> sending notification............')


