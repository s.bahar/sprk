from django.contrib.auth.models import Group

from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from .serializers import UserProfile, UserProfileForSuopport, AddressSerializers
from .models import User


@api_view(['GET'])
def user_list(request):

    if not Group.objects.get(name='support').user_set.filter(id=request.user.id).exists():

        queryset = User.objects.all()
        serializer = UserProfile(queryset, many=True)
        return Response(serializer.data)

    else:

        if 'lt' in request.GET.keys():
            queryset = User.objects.n_addresses_less(request.GET['lt'])
        elif 'gt' in request.GET.keys():
            queryset = User.objects.n_addresses_greater(request.GET['gt'])
        elif 'eq' in request.GET.keys():
            queryset = User.objects.n_addresses_equal(request.GET['eq'])
        else:
            queryset = User.objects.all()

        serializer = UserProfileForSuopport(queryset, many=True)
        return Response(serializer.data)


@api_view(['POST'])
def add_address(request):

    if not Group.objects.get(name='support').user_set.filter(id=request.user.id).exists():
        return Response({'message':'go away!!!'}, status=status.HTTP_403_FORBIDDEN)
    else:
        serializer = AddressSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
